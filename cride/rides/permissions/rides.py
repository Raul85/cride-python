"""Rides permissions"""

# Django REST Framework
from rest_framework.permissions import BasePermission


class IsRideOwner(BasePermission):
    """Verificar que el usuario solicitante sea que creo el viaje"""

    def has_object_permission(self, request, view, obj):
       
        return request.user == obj.offered_by


class IsNotRideOwner(BasePermission):
    """Solo los usuarios que no son propietarios del viaje pueden llamar a la vista"""

    def has_object_permission(self, request, view, obj):
        
        return not request.user == obj.offered_by