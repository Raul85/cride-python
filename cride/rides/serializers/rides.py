"""Rides serializers"""

# Django REST Framework
from rest_framework import serializers

# Models
from cride.circles.models import Membership
from cride.rides.models import Ride
from cride.users.models import User

# Serializers
from cride.users.serializers import UserModelSerializer

# Utilities
from datetime import timedelta
from django.utils import timezone


class RideModelSerializer(serializers.ModelSerializer):
    
    offered_by = UserModelSerializer(read_only=True)
    offered_in = serializers.StringRelatedField()

    passengers = UserModelSerializer(read_only=True, many=True)

    class Meta:
        
        model = Ride
        fields = '__all__'
        read_only_fields = (
            'offered_by',
            'offered_in',
            'rating'
        )

    def update(self, instance, data):
        """permitir actualizaciones antes de la fecha"""
        
        now = timezone.now()
        if instance.departure_date <= now:
            raise serializers.ValidationError('Los viajes en curso no se puden modificar')
        return super(RideModelSerializer, self).update(instance, data)


class CreateRideSerializer(serializers.ModelSerializer):
    """Create ride serializer."""

    offered_by = serializers.HiddenField(default=serializers.CurrentUserDefault())
    available_seats = serializers.IntegerField(min_value=1, max_value=15)

    class Meta:
       
        model = Ride
        exclude = ('offered_in', 'passengers', 'rating', 'is_active')

    def validate_departure_date(self, data):
        """verificar que la fecha no se ha pasado"""
        
        min_date = timezone.now() + timedelta(minutes=10)
        if data < min_date:
            raise serializers.ValidationError(
                'Hora de salida debe de pasar al menos 20 minutos'
            )
        return data

    def validate(self, data):
        
        if self.context['request'].user != data['offered_by']:
            raise serializers.ValidationError('viajes ofrecidos en nombre de otras personas no esta permitido')

        user = data['offered_by']
        circle = self.context['circle']
        try:
            membership = Membership.objects.get(
                user=user,
                circle=circle,
                is_active=True
            )
        except Membership.DoesNotExist:
            raise serializers.ValidationError('Usuario no esta activo en el circulo')

        if data['arrival_date'] <= data['departure_date']:
            raise serializers.ValidationError('Fecha de salida debe ser posterior a la llegada')

        self.context['membership'] = membership
        return data

    def create(self, data):
        """Create ride and update stats."""
        
        circle = self.context['circle']
        ride = Ride.objects.create(**data, offered_in=circle)

        # Circle
        circle.rides_offered += 1
        circle.save()

        # Membership
        membership = self.context['membership']
        membership.rides_offered += 1
        membership.save()

        # Profile
        profile = data['offered_by'].profile
        profile.rides_offered += 1
        profile.save()

        return ride


class JoinRideSerializer(serializers.ModelSerializer):
    """Join ride serializer."""

    passenger = serializers.IntegerField()

    class Meta:
      
        model = Ride
        fields = ('passenger',)

    def validate_passenger(self, data):
        """Verificar que exista el pasajero y esta en el circulo"""
        
        try:
            user = User.objects.get(pk=data)
        except User.DoesNotExist:
            raise serializers.ValidationError('Pasajero invalido')

        circle = self.context['circle']
        try:
            membership = Membership.objects.get(
                user=user,
                circle=circle,
                is_active=True
            )
        except Membership.DoesNotExist:
            raise serializers.ValidationError('El usuario no es un miembro activo en el circulo')

        self.context['user'] = user
        self.context['member'] = membership
        return data

    def validate(self, data):
        """Verificar que los viajes permitan nuevos pasajeros"""
        
        ride = self.context['ride']
        if ride.departure_date <= timezone.now():
            raise serializers.ValidationError("No puede unirse en este viaje ahora")

        if ride.available_seats < 1:
            raise serializers.ValidationError("El viaje esta lleno!")

        if ride.passengers.filter(pk=self.context['user'].pk).exists():
            raise serializers.ValidationError('El pasajero ya esta en viaje')

        return data

    def update(self, instance, data):
        """Add passenger to ride, and update stats."""
        
        ride = self.context['ride']
        user = self.context['user']

        ride.passengers.add(user)

        # Profile
        profile = user.profile
        profile.rides_taken += 1
        profile.save()

        # Membership
        member = self.context['member']
        member.rides_taken += 1
        member.save()

        # Circle
        circle = self.context['circle']
        circle.rides_taken += 1
        circle.save()

        return ride


class EndRideSerializer(serializers.ModelSerializer):
    """End ride serializer."""

    current_time = serializers.DateTimeField()

    class Meta:
        
        model = Ride
        fields = ('is_active', 'current_time')

    def validate_current_time(self, data):
        """Verificar que el viaje haya comenzado"""
        
        ride = self.context['view'].get_object()
        if data <= ride.departure_date:
            raise serializers.ValidationError('El viaje no ha iniciado')
        return data
