"""Membership model"""

# Django
from django.db import models

# Utilities
from cride.utils.models import CRideModel

class Membership(CRideModel):
    """Membership model
    
    Una membresia tiene relacion entre un usuario y un circulo
    
    """

    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    profile = models.ForeignKey('users.Profile', on_delete=models.CASCADE)
    circle = models.ForeignKey('circles.Circle', on_delete=models.CASCADE)

    is_admin = models.BooleanField(
        'circle_admin',
        default=False,
        help_text='Los administradores del circulo pueden actualizar los datos'
    )

    used_invitations = models.PositiveSmallIntegerField(default=0)
    remaining_invitations = models.PositiveSmallIntegerField(default=0)
    invited_by = models.ForeignKey(
        'users.User',
        null=True,
        on_delete=models.SET_NULL,
        related_name='invited_by'
    )

    # Stats
    rides_taken = models.PositiveSmallIntegerField(default=0)
    rides_offered = models.PositiveSmallIntegerField(default=0)

    # Status
    is_active = models.BooleanField(
        'active status',
        default=True,
        help_text='Solamente los usuarios activos pueden interactuar en el circulo'
    )

    def __str__(self):
        """Return username and circle"""
        return '@{} at #{}'.format(
            self.user.username,
            self.circle.slug_name
        )

