"""Circle modal"""

#Django
from django.db import models

#Utilities
from cride.utils.models import CRideModel

class Circle(CRideModel):
    """Circle model

    Circulo privado de grupo
    """

    name = models.CharField('circle name', max_length=140)
    slug_name = models.SlugField(unique=True, max_length=40)

    about = models.CharField('circle descripcion', max_length=255)
    picture = models.ImageField(upload_to='circles/pictures', blank=True, null=True)

    members = models.ManyToManyField(
        'users.User', 
        through='circles.Membership',
        through_fields=('circle','user')
    )

    #stats
    rides_offered = models.PositiveIntegerField(default=0)
    rides_taken = models.PositiveIntegerField(default=0)

    verified = models.BooleanField(
        'verified circle',
        default=False,
        help_text='Verificado por la comunidad'
    )

    is_public = models.BooleanField(
        default=True,
        help_text='Circulos listados como publicos' 
    )

    is_limited = models.BooleanField(
        'limited',
        default=False,
        help_text='Limite de usuarios en el grupo'
    )

    members_limit = models.PositiveIntegerField(
        default=0,
        help_text='Limite de usuarios en el grupo'
    )

    def __str__(self):
        """Retorna el nombre del circulo"""
        return self.name
    
    class Meta(CRideModel.Meta):
        """Meta class"""

        ordering = ['-rides_taken','-rides_offered']
