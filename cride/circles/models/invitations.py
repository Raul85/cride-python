"""Circle invitations models"""

# Django
from django.db import models

# Utilities
from cride.utils.models import CRideModel

# Managers
from cride.circles.managers import InvitationManager


class Invitation(CRideModel):
    """Circle invitation"""

    code = models.CharField(max_length=50, unique=True)

    issued_by = models.ForeignKey(
        'users.User',
        on_delete=models.CASCADE,
        help_text='Miembro del circulo que proporciona invitacion',
        related_name='issued_by'
    )
    used_by = models.ForeignKey(
        'users.User',
        on_delete=models.CASCADE,
        null=True,
        help_text='Usuario que utiliza el codigo para ingresar la circulo'
    )

    circle = models.ForeignKey('circles.Circle', on_delete=models.CASCADE)

    used = models.BooleanField(default=False)
    used_at = models.DateTimeField(blank=True, null=True)

    # Manager
    objects = InvitationManager()

    def __str__(self):
        """Retorna el codigo del circulo"""
        return '#{}: {}'.format(self.circle.slug_name, self.code)