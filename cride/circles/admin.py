"""Circles admin"""

#Django
from django.contrib import admin

#Models
from cride.circles.models import Circle

@admin.register(Circle)
class CircleAdmin(admin.ModelAdmin):
    """Circle admin"""

    list_display = (
        'slug_name',
        'name',
        'is_public',
        'verified',
        'is_limited',
        'members_limit'
    )

    search_fields = ('slug_name','name')

    list_filter = (
        'is_public',
        'verified',
        'is_limited'
    )
    

    actions = ['make_verified','make_unverified']

    def make_verified(self, request, queryset):
        queryset.update(verified=True)
    make_verified.short_description = 'Seleccionar los circulos verificados'

    def make_unverified(self, request, queryset):
        queryset.update(verified=False)
    make_unverified.short_description = 'Seleccionar los circulos sin verificar'

    
    def download_todays_rides(self, request, queryset):
        """Viajes del dia"""
        
        now = timezone.now()
        start = datetime(now.year, now.month, now.day, 0, 0, 0)
        end = start + timedelta(days=1)
        rides = Ride.objects.filter(
            offered_in__in=queryset.values_list('id'),
            departure_date__gte=start,
            departure_date__lte=end
        ).order_by('departure_date')

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="rides.csv"'
        writer = csv.writer(response)
        writer.writerow([
            'id',
            'passengers',
            'departure_location',
            'departure_date',
            'arrival_location',
            'arrival_date',
            'rating',
        ])
        for ride in rides:
            writer.writerow([
                ride.pk,
                ride.passengers.count(),
                ride.departure_location,
                str(ride.departure_date),
                ride.arrival_location,
                str(ride.arrival_date),
                ride.rating,
            ])

        return response
    download_todays_rides.short_description = 'Descarga los viajes del dia'