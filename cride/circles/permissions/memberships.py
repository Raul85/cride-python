"""Circles permission classes"""

# Django REST framework
from rest_framework.permissions import BasePermission

# Models
from cride.circles.models import Membership

class IsActiveCircleMember(BasePermission):
    """permitir el acceso solamante a los miembros del circulo"""

    def has_permission(self, request, view):
        """verificar si el usuario es activo en el circulo"""
        try:
            Membership.objects.get(
                user=request.user,
                circle=view.circle,
                is_active=True
            )
        except Membership.DoesNotExist:
            return False
        return True

class IsSelfMember(BasePermission):
    """permite el acceso solamente a los propietarios"""

    def has_permission(self, request, view):
        """permite que el objeto otorque acceso"""
        obj = view.get_object()
        return self.has_object_permission(request, view, obj)

    def has_object_permission(self, request, view, obj):
        """permitir si solo el miembro es propietario"""
        return request.user == obj.user