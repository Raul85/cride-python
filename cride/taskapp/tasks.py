"""Celery task"""


# Django
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone

# Celery
from celery.decorators import task, periodic_task

# Models
from cride.users.models import User
from cride.rides.models import Ride

# Utilities
import jwt
import time
from datetime import timedelta
    
def gen_verification_token(user):
    """Create JWT token"""
    exp_date = timezone.now() + timedelta(days=3)
    payload = {
        'user': user.username,
        'exp': int(exp_date.timestamp()),
        'type': 'email_confirmation'
    }
    token = jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256')
    return token

@task(name='send_confirmation_email', max_retries=3)
def send_confirmation_email(user_pk):
    """Envio de link para verficacion de usuario"""
    for i in range(30):
        time.sleep(1)
        print("...", str(i+1))
    user = User.objects.get(pk=user_pk)
    verification_token = gen_verification_token(user)
    subject = 'Bienvenido @{}! Verificar tu cuenta para utilizar la aplicacion '.format(user.username)
    from_email = 'Comparte Ride <noreply>@comparteride.com'
    content = render_to_string(
       'emails/users/account_verification.html',
       {'token': verification_token, 'user':user}
    )
    msg = EmailMultiAlternatives(subject,content,from_email,[user.email])
    msg.attach_alternative(content, "text/html")
    msg.send()


@periodic_task(name='disable_finished_rides', run_every=timedelta(minutes=20))
def disable_finished_rides():
    """Disable finished rides."""
    now = timezone.now()
    offset = now + timedelta(minutes=20)

    # actualizar si realmente termino el viaje
    rides = Ride.objects.filter(
        arrival_date__gte=now,
        arrival_date__lte=offset,
        is_active=True
    )
    rides.update(is_active=False)
   