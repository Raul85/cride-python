"""Django models utilities"""

#Django
from django.db import models

class CRideModel(models.Model):
    """Comparte Ride base model
    
        created (DateTime): Store the datetime the object was created
        modified (DateTime): Store the datetime the object was modified
    """

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the object was created'
    )
    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Date time on which the object was las modified'
    )

    class Meta:
        """Meta Opcion"""
        abstract = True

        get_latest_by = 'created'
        ordering = ['-created','-modified']
        

