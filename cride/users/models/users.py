"""User model"""

# Django
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator

# Utilities
from cride.utils.models import CRideModel

class User(CRideModel, AbstractUser):
    """User model

        Extension Django AbstracUser, cambiar campo usuario por email y
        agregar campos adicionales
    """

    email = models.EmailField(
        'email address',
        unique=True,
        error_messages={
            'unique': 'El usuario ya existe'
        }
    )

    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message="El formata para el numero telefonico: +999999999999999 15 digitos"
    )

    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username','first_name','last_name']

    is_client = models.BooleanField(
        'client status',
        default=True,
        help_text=(
            'Nivel de permiso del usuario'
        )
    )

    is_verified = models.BooleanField(
        'verified',
        default=True,
        help_text='Correo de usuario verificado'
    )

    def __str__(self):
        """Retorna username"""
        return self.username

    def get_short_name(self):
        """Retorna username"""
        return self.username

    