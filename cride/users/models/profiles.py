"""Profile model"""

# Django
from django.db import models

# utilities
from cride.utils.models import CRideModel


class Profile(CRideModel):
    """Modelo de perfil de usuario"""

    user = models.OneToOneField('users.User', on_delete=models.CASCADE)

    picture = models.ImageField(
        'profile picture',
        upload_to='users/pictures/',
        blank=True,
        null=True
    )

    biography = models.TextField(max_length=500, blank=True)

    # Stats
    rides_taken = models.PositiveIntegerField(default=0)
    rides_offered = models.PositiveIntegerField(default=0)
    reputation = models.FloatField(
        default = 5.0,
        help_text='Calificación en la reputacion del cliente'
    )

    def __str__(self):
        """retornar string del user"""
        return str(self.user)

