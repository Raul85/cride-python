"""Profile serializers"""

# Django REST Framework
from rest_framework import serializers

# Models
from cride.users.models import Profile

class ProfileModelSerializer(serializers.ModelSerializer):

    class Meta:

        model = Profile
        fields = (
            'picture',
            'biography',
            'rides_taken',
            'rides_offered',
            'reputation'
        ) 
        # campos que no pueden ser modificados por el usuario
        read_only_fields = (
            'rides_taken',
            'rides_offered',
            'reputation'  
        )
        